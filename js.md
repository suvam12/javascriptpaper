# BASIC DATATYPE AND DATA STRUCTURE IN JAVA SCRIPT

## DATA TYPES

1)*String* eg: var x="Hello World";

2)*Number* eg:var x=67;

3)*Boolean* eg:var x=true;

4)*Array* eg:var x=["x","y","z"];

5)*Object* eg:{firstN:"rohit",lastN:"singh"}

6)*null* eg:var x=null;

7)*Undefined* eg:var x;
___

## DATA STRUCTURE

Data structure allows you to manage data, different task required different data structure .

### COMMON DATA STRUCTURE 

1)*Array* :  Odered list of data , duplicates are allowed

2)*Set*: Unordered list of data where no duplicateds are required

3)*Object*: Key-Value pair of unordered data.

4)*Map*: Key-Value Ordered data

1) ### ARRAY
a)  Insertion order is kept

 b) Elements accessed via index

 c) Iterable by for loop

 d) Size of array adjust dynamically, it grows and srinks 

 e) Duplicate value allowed

 eg: cons names=["max","menue","julie","max"];

    console.log(names[0]);
    for(const N of names){
        console.log(N)
    }
    name.push('julie');
    console.log(names.length);
    name.split(2,1);//remove 1 element after 2

2) ### SETS

1.Duplicates are not allowed if we try to add will not get any error but item will not be added

2.Insertion order is not preserved

3.Access element via method

4.Can iterate via for loopp

5.Size and Length adjusted dynamically

6.Deletion and finding elements is faster and performance is better than array

const num=new Set(["abc","def","ghi"]);

num.add("jkl");

num.add("xyz");

num.add("qrs");

console.log(num.has('abc'))//boolean


3. ### OBJECTS

1.It will store ubordered key-value pair

2.Elements accessed via key

3.Not iterable except by for-in loop

4.Keys are unique,but values can be duplicated

5.Keys have to be string,number, arrays or objects are not allowed

6.Constructor functons can also be used to create objects

const human={firstN:"mohit" lastN:"roy",hobbies:[cooking,planting]}

console.log(human['firstN']);

console.log(human.firstN);

delete person.age;

4. ### MAP

1.Key value pairs, with insertion order is preserved.

2.Elements acess via keys.

3.Maps are iterable we can use for-of loop.

4.Keys are unique, but values are not, if we try to insert same key then it will not give any error but replace old key value pair.

5.Keys can be anything including arrays and objects.

6.We can not use dot notation to get obejct.

eg: const result=new Map();

result.set('average',1.53);

result.set('last Result',null);

const place={name:'goa',population:'8.2'}

result.set(place,100);

for(const M of result){
    console.log(M);
}

result.delete(place);

## VARIATION OF SET AND MAP

a.
### WEAK MAP:  Keys are weakly referenced

- If java Script finds out that data structure is used no where in your code than in such case it will free up that space in memory, for weak set and weak map it will happen automaticaly.

b.
### LINKED LIST

-  Every node in linkedList has its value as well as a pointers to its next element but don't know about its previous node.

- Flexible and memory efficient and can wasily insert element in middle.

class LinkedList{

    constructor(){
        this.head=null; //first element in list

        this.tail=null;//last element in list
    }
}

prepand(value){

    const newNode={value:value,next: this.head};
    
    this.head=neewNode;

    if(!this.head){
        this.head=newNode;
    }
}

append(value){

    const newNode={value:value,next: null};

    if(this.tail){
        this.tail.next=newNode;
    }
    this.tail=newNode;
    if(!this.head){
        this.head=newNode;
    }
}

const linkedList=new LinkedList();

linkedList.append(4);

linkedList.append("hello world");
linkedList.append("hello java");

linkedList.prepend(58.8);

linkedList.prepend("hello world again");